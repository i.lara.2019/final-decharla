# Final DeCharla

# ENTREGA CONVOCATORIA MAYO

# ENTREGA DE PRÁCTICA

## Datos

* Nombre: Iván Lara de Mena
* Titulación: Tecnologías de la Telecomunicación 
* Cuenta en laboratorios: ivanlara
* Cuenta URJC: i.lara.2019@alumnos.urjc.es
* Video básico (url): https://youtu.be/YKb0ZM5TB1k
* Video parte opcional (url): https://youtu.be/UKNhyCW8WCs
* Despliegue (url): https://ivanlara.pythonanywhere.com
* Contraseñas: ivanlarademena/ivanlarademena
* Cuenta Admin Site: ivanlara/Larademena@07

## Resumen parte obligatoria
He tratado de cumplir todas las funcionalidades obligatorias según pedía el enunciado, aunque mi práctica
tiene una particularidad con respecto a la funcionalidad obligatoria. A la hora de loguearse, mi aplicación no solo 
te pide una contraseña, sino que, también te pide ingresar un usuario válido (puedes registrarte), es decir,
en mi aplicación necesitas estar registrado para poder acceder a los contenidos de la misma. Para hacer esto, NO HE UTILIZADO
LAS CUENTAS PROPORCIONADAS POR DJANGO, sino que, he implementado mi propia lógica de autenticación, verificando que en cada recurso
que exista una cookie de sesión con un nombre asignado, si no lo hay, te redirige a la página de inicio de sesión que tiene una
pestaña para que el usuario pueda loguearse. En la página de inicio de sesión el código que he elaborado compara el
valor introducido en el formulario con lo que hay creado en la base de datos. (Lo hago a mano sin utilizar cuentas de django, solo 
utilizo la funcionalidad de django de cuentas para comparar y crear contraseñas en formato cifrado).
Si el valor introducido coincide con lo almacenado, al usuario se le proporciona una cookie de sesión, que 
cada recurso buscará al inicializarse su código (Como explicaba antes). Además con esa cookie extraigo el nombre del usuario de la base de datos.

El motivo por el cual he utilizado cuentas, es que mi plan inicial era implementar una autenticación biométrica del usuario, analizando el patrón de escritura.
No veía viable para implementar esto autenticar navegadores por medio de una cookie de sesión que va cambiando, además de no tener usuarios o que fueran elegidos de forma
aleatoria sin estar ligados a una cuenta.

-Si nos centramos en el resto de la funcionalidad, el enunciado se cumple al pie de la letra.
Cualquier usuario puede crear una sala (una vez autenticado) e interactuar en ella enviando tanto texto cómo imágenes.
En la página principal se le mostrará al usuario todas las salas a las que ha mandado información. En la pagina de cada sala
se podrán ver todos los mensajes e imágenes ordenados. Además, puede ver
el contenido de las salas de forma dinámica (se actualiza cada 30 segundos) si accede al recurso /nombresala/din.

-En cuanto a las funcionalidades relacionadas con xml y json, puede verse la información de cada sala en formato json
en el recurso /nombresala.json y se puede actualizar la información haciendo un put al recurso 
/nombresala/xml siempre y cuando el xml tenga el formato correcto e incluya la contrseña pedida.

-Hay un recurso llamado /configuración que permite cambiar el nombre del usuario y el formato de la letra (tamaño y tipo)

-En el recurso /ayuda se provee al usuario de una breve explicación de como funciona el sitio y una breve documentacion.

-En el recurso /admin del menú se accede al modo administrador, visualizando los nombres de cada objeto de la base
de datos

-Se han realizado los test de extremo a extremo pertinentes para garantizar la funcionalidad de la práctica.


## Lista partes opcionales

* Favicon: Inclusión del logo de la app en forma de favincon.
* Cerrar sesión: El servidor mediante un botón es capaz de eliminar las cookies 
de sesión del usuario, tanto en el navegador como en el servidor. Fuerza que el cliente tenga que volver
a hacer login para acceder a recursos. 
* API TypingDNA (Solo permite usuarios de 6-256 carácteres): El servidor se pone en contacto con una api externa, 
para autenticar al usuario mediante patrones de escritura. Al activarlo, y una vez autenticado, se desbloquea un pequeño easter-egg
que cambia el fondo de pantalla a un color azul claro. 
* Registro: La apicación permite registrar nuevos usuarios.