from django.db import models

class User(models.Model):
    User = models.CharField(max_length=64)
    Password = models.TextField(max_length=64)

    def __str__(self):
        return self.User


class Session(models.Model):
    Cookie = models.CharField(max_length=64, null=True)
    User = models.ForeignKey('User', on_delete=models.CASCADE)

    def __str__(self):
        return self.Cookie


class Sala(models.Model):
    Salas = models.CharField(max_length=64, null=True)
    Date = models.DateTimeField()
    Author = models.ForeignKey('User', on_delete=models.CASCADE)

    def __str__(self):
        return self.Salas


class SMS(models.Model):
    Mensajes = models.TextField()
    Author = models.ForeignKey('User', on_delete=models.CASCADE)
    Date = models.DateTimeField()
    Sala = models.ForeignKey('Sala', on_delete=models.CASCADE)
    Imagen = models.ImageField(upload_to='sms_images/', null=True, blank=True)


class UserSala(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    sala = models.ForeignKey(Sala, on_delete=models.CASCADE)
    last_visited = models.DateTimeField()

    def __str__(self):
        return f"User: {self.user.User}, Sala: {self.sala.Salas}"  # Adjust this as needed



