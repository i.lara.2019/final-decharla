# Generated by Django 3.1.7 on 2023-05-22 14:42

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('DeCharla', '0012_auto_20230522_1424'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='Cookie',
        ),
        migrations.CreateModel(
            name='Session',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Cookie', models.CharField(max_length=64, null=True)),
                ('User', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='DeCharla.user')),
            ],
        ),
    ]
