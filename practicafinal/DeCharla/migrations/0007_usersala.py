# Generated by Django 3.1.7 on 2023-05-21 18:29

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('DeCharla', '0006_auto_20230521_1715'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserSala',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('last_visited', models.DateTimeField(auto_now=True)),
                ('sala', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='DeCharla.sala')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='DeCharla.user')),
            ],
        ),
    ]
