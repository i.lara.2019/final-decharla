from django.contrib import admin
from .models import User, Sala, SMS, UserSala

# Register your models here.
admin.site.register(User)
admin.site.register(Sala)
admin.site.register(SMS)
admin.site.register(UserSala)
