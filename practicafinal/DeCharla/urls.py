from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('favicon.jpg', views.get_favicon, name='favicon'),
    path('', views.principal, name='principal'),
    path('<str:id_pagina>/', views.salas, name='salas'),
    path('ayuda', views.help),
    path('configuracion', views.config),
    path('login', views.login, name='login'),
    path('register', views.register, name='register'),
    path('logout', views.logout),
    path('typing', views.typing),
    path('<str:id_pagina>.json', views.arc_json, name='json'),
    path('<str:id_pagina>/xml', views.xml, name='xml'),
    path('<str:id_pagina>/din', views.dinamica, name='saladinamica'),
]+static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
