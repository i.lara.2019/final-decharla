from django.test import TestCase, RequestFactory
from django.shortcuts import reverse
from django.contrib.auth.hashers import make_password
from .models import User
from .views import login, register, principal

class UserAuthenticationTestCase(TestCase):
    def setUp(self):
        self.factory = RequestFactory()

    def test_register_new_user(self):
        request = self.factory.post(reverse('register'), {'username': 'newuser', 'password1': 'password123', 'password2': 'password123', 'checkbox': 'on'})
        response = register(request)
        self.assertEqual(response.status_code, 302)  # Debería haber una redirección
        self.assertEqual(response.url, '/typing')  # La redirección debería ser a '/typing'
        user = User.objects.filter(User='newuser').first()
        self.assertIsNotNone(user)  # El nuevo usuario debería haberse creado

    def test_register_existing_user(self):
        User.objects.create(User='existinguser', Password=make_password('password123'))  # Creamos un usuario existente
        request = self.factory.post(reverse('register'), {'username': 'existinguser', 'password1': 'password123', 'password2': 'password123', 'checkbox': 'on'})
        response = register(request)
        self.assertEqual(response.status_code, 200)  # El formulario de registro debería volverse a mostrar
        self.assertContains(response, 'Error: El usuario ya existe')  # Debería haber un mensaje de error

    def test_register_password_mismatch(self):
        request = self.factory.post(reverse('register'), {'username': 'newuser', 'password1': 'password123', 'password2': 'wrongpassword', 'checkbox': 'on'})
        response = register(request)
        self.assertEqual(response.status_code, 200)  # El formulario de registro debería volverse a mostrar
        self.assertContains(response, 'Error: Las contraseñas no coinciden')  # Debería haber un mensaje de error

    def test_login_existing_user(self):
        User.objects.create(User='existinguser', Password=make_password('password123'))  # Creamos un usuario existente
        request = self.factory.post(reverse('login'), {'username': 'existinguser', 'password': 'password123', 'checkbox': 'on'})
        response = login(request)
        self.assertEqual(response.status_code, 302)  # Debería haber una redirección
        self.assertEqual(response.url, '/typing')  # La redirección debería ser a '/typing'

    def test_login_non_existing_user(self):
        request = self.factory.post(reverse('login'), {'username': 'nonexistinguser', 'password': 'password123', 'checkbox': 'on'})
        response = login(request)
        self.assertEqual(response.status_code, 200)  # El formulario de inicio de sesión debería volverse a mostrar
        self.assertContains(response, 'Error: Usuario o contraseña incorrectos')  # Debería haber un mensaje de error

    def test_login_wrong_password(self):
        User.objects.create(User='existinguser', Password=make_password('password123'))  # Creamos un usuario existente
        request = self.factory.post(reverse('login'), {'username': 'existinguser', 'password': 'wrongpassword', 'checkbox': 'on'})
        response = login(request)
        self.assertEqual(response.status_code, 200)  # El formulario de inicio de sesión debería volverse a mostrar
        self.assertContains(response, 'Error: Usuario o contraseña incorrectos')  # Debería haber un mensaje de error

class PrincipalViewTestCase(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create(User='existinguser', Password=make_password('password123'))  # Creamos un usuario existente

    def test_principal_view_GET(self):
        request = self.factory.get(reverse('principal'))
        request.user = self.user
        response = principal(request)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, '/login')

    def test_principal_view_POST_new_sala(self):
        request = self.factory.post(reverse('principal'), {'nombre-sala': 'new_sala'})
        request.user = self.user
        response = principal(request)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, '/login')

    def test_principal_view_POST_no_sala_name(self):
        request = self.factory.post(reverse('principal'), {'nombre-sala': ''})
        request.user = self.user
        response = principal(request)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, '/login')

    def test_principal_view_disallowed_method(self):
        request = self.factory.put(reverse('principal'))  # Método no permitido
        request.user = self.user
        response = principal(request)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, '/login')

