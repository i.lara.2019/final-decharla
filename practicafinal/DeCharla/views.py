from django.contrib.auth.hashers import check_password
from django.utils import timezone
from .models import Session, User, SMS, Sala, UserSala
from django.shortcuts import redirect
from django.contrib.auth.hashers import make_password
import string, random
from django.http import HttpResponseNotFound, HttpResponseNotAllowed, HttpResponseRedirect, HttpResponse, HttpResponseForbidden
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
import urllib.request, base64
from urllib.error import HTTPError
from django.core.files.storage import default_storage
from django.http import JsonResponse
import xml.etree.ElementTree as ET
from django.http import HttpResponseBadRequest
import json

def get_favicon(request):
    favicon = open("favicon.jpg", "rb")
    ans = favicon.read()
    return HttpResponse(ans)

@csrf_exempt
def typing(request):
    try:
        user = some_view_requiring_login(request)
        if isinstance(user, HttpResponseRedirect):  # verifica si la variable user es una instancia de la clase HttpResponseRedirect
            return user  # Redirige directamente a la página de inicio de sesión
        info = barrainf()
        API_KEY = "fd7ae763e838ce01dbf24696d3c23f7e"
        API_SECRET = "d4e81b1b61cf3bb68ef5147a5e6693da"
        if request.method == 'POST':
                    username = user.User
                    tp = request.POST.get('typingPattern')
                    text = request.POST.get('frase')
                    mensaje = 'ESTA PRACTICA ES SUPER GENIAL Y GRANDE'
                    if text != mensaje:
                        msg = 'Texto para analizar erroneo escribalo correctamente'
                        contex = {
                            'msg': msg,
                            **info
                        }
                        return render(request, 'typingDNA.html', contex)
                    base_url = 'https://api.typingdna.com'
                    authstring = '%s:%s' % (API_KEY, API_SECRET)
                    base64string = base64.b64encode(authstring.encode()).decode().replace('\n', '')
                    data = urllib.parse.urlencode({'tp': tp})
                    url = '%s/auto/%s' % (base_url, username)
                    req = urllib.request.Request(url, data.encode('utf-8'), method='POST')
                    req.add_header("Authorization", "Basic %s" % base64string)
                    req.add_header("Content-type", "application/x-www-form-urlencoded")
                    try:
                        res = urllib.request.urlopen(req)
                        res_body = res.read()
                        response_data = json.loads(res_body.decode('utf-8'))
                        print(response_data)

                        if response_data['message_code'] == 10:
                            return redirect('/typing')
                        if response_data['message_code'] == 1:
                            msg = f'Enhorabuena, eres tú {user.User}, según tu patrón de escritura, hemos podido identificarte'
                            request.session['typing_authenticated'] = True
                            contex = {
                                'msg': msg,
                                **info
                            }
                            return render(request, 'typingDNA.html', contex)
                        if response_data['message_code'] == 6:
                            msg = f'No eres tú {user.User}, según tu patrón de escritura, no hemos podido identificarte'
                            contex = {
                                'msg': msg,
                                **info
                            }
                            return render(request, 'typingDNA.html', contex)
                    except HTTPError as e:
                            msg = f'Error: O tú nombre {user.User}, no es válido, debe tener de 6 a 256 carácteres de longitud puedes cambiarlo en la ventana de configuracion o estas tratando de copiar y pegar.'
                            contex = {
                                'msg': msg,
                                **info
                            }
                            return render(request, 'typingDNA.html', contex)
        else:
            return render(request, 'typingDNA.html', info)
    except ValueError as e:
        msg = f'Error: {str(e)}'  # Mensaje de error personalizado
        context = {
            'msg': msg,
            **info
        }
        return render(request, 'typingDNA.html', context)

def barrainf():
    num_salas = Sala.objects.count()
    num_msg = SMS.objects.count()
    num_fotos = SMS.objects.exclude(Imagen__isnull=True).exclude(Imagen='').count()
    return {'num_salas': num_salas, 'num_fotos': num_fotos, 'num_msg': num_msg}

def register(request):
    info = barrainf()
    # Comprobar si ya existe una cookie de sesión
    session_cookie = request.COOKIES.get('session')
    session = Session.objects.filter(Cookie=session_cookie).first()
    if session_cookie and session:
        return render(request, 'logeado.html')
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password1')
        password_comp = request.POST.get('password2')
        typing = request.POST.get('checkbox')
        # Comprobar si el usuario ya existe
        if User.objects.filter(User=username).exists():
            contex = {
                'error': 'Error: El usuario ya existe',
                **info
            }
            return render(request, 'register.html', contex)
        if password != password_comp:
            contex = {
                'error': 'Error: Las contraseñas no coinciden',
                **info
            }
            return render(request, 'register.html', contex)
        # Crear un nuevo usuario
        user = User(User=username, Password=make_password(password))
        user.save()
        session = Session.objects.create(User=user, Cookie=''.join(random.choices(string.ascii_lowercase + string.digits, k=128)))
        session.save()
        if typing == "on":
            response = redirect('/typing')
            response.set_cookie('session', session.Cookie)
            return response #si se activa el registro en typing el usuario puede elegir validarse con typing
        # Redirigir al usuario a la página principal
        response = redirect('/')
        response.set_cookie('session', session.Cookie)
        return response
    else:
        return render(request, 'register.html')

def dinamica(request, id_pagina):
    user = some_view_requiring_login(request)
    if isinstance(user,HttpResponseRedirect):  # verifica si la variable user es una instancia de la clase HttpResponseRedirect
        return user
    info = barrainf()
    contex = {
        'id_pagina': id_pagina,
        **info
    }
    return render(request, 'saladin.html', contex)

def login(request):
    info = barrainf()
    # Comprobar si ya existe una cookie de sesión
    session_cookie = request.COOKIES.get('session')
    session = Session.objects.filter(Cookie=session_cookie).first()
    if session_cookie and session:
        return render(request, 'logeado.html', info)
    # Procesar formulario de inicio de sesión
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        typing = request.POST.get('checkbox')
        user = User.objects.filter(User=username).first()
        if user and check_password(password, user.Password):
            # Si el usuario y la contraseña son correctos, crea una nueva sesión
            session = Session.objects.create(User=user, Cookie=''.join(
                random.choices(string.ascii_lowercase + string.digits, k=128)))
            response = redirect('/')  # redirige al usuario a la página principal
            response.set_cookie('session', session.Cookie)  # agrega la cookie de la sesión al HttpResponse
            if typing == "on":
                response = redirect('/typing')
                response.set_cookie('session', session.Cookie)
                return response  # si se activa el registro en typing el usuario puede elegir validarse con typing
            return response
        else:
            # Si el usuario o la contraseña son incorrectos, muestra un error
            contex = {
                'error': 'Error: Usuario o contraseña incorrectos',
                **info
            }
            return render(request, 'login.html', contex)
    else:
        return render(request, 'login.html', info)

@csrf_exempt
def xml(request, id_pagina):
    try:
        sala = Sala.objects.get(Salas=id_pagina)
    except Sala.DoesNotExist:
        return HttpResponseNotFound('<h1>Página no encontrada</h1>')
    # Verificar la cabecera de autorización
    authorization_header = request.META.get('HTTP_AUTHORIZATION')
    if not authorization_header or authorization_header != 'Basic xx34d23':
        return HttpResponseForbidden('Error: Cabecera de autorización no válida')
    # Obtener o crear un usuario anónimo
    anonymous_user, _ = User.objects.get_or_create(User='anonymous', Password='xx34d23')
    if request.method == 'PUT':
        try:
            xml_data = request.body
            root = ET.fromstring(xml_data)
            for message_elem in root.iter('message'):
                is_img = message_elem.get('isimg', 'false') == 'true'
                text_elem = message_elem.find('text')
                if text_elem is not None:
                    text = text_elem.text.strip()
                    if text:
                        nuevo_mensaje = SMS(Author=anonymous_user, Mensajes=text, Sala=sala, Date=timezone.now(),
                                            Imagen='' if is_img else None)
                        nuevo_mensaje.save()
        except ET.ParseError:
            return HttpResponseBadRequest('Error: El formato del documento XML es inválido')
        return HttpResponse('Mensajes agregados exitosamente')
    else:
        return HttpResponseNotAllowed(['PUT'])

def logout(request):
    # Obtén la cookie de sesión de la solicitud
    session_cookie = request.COOKIES.get('session')
    # Comprueba si existe la cookie de sesión
    if session_cookie is not None:
        # Si existe, busca la sesión en la base de datos
        session = Session.objects.filter(Cookie=session_cookie).first()
        # Compruebo si existe la sesión
        if session is not None:
            # Si existe, elimino la sesión de la base de datos
            Session.objects.filter(Cookie=session_cookie).delete()
            response = redirect('/login')
            # Elimina la cookie de sesión del navegador
            response.delete_cookie('session')
            request.session['typing_authenticated'] = False
            return response
    # Si no existe la cookie de sesión o la sesión, redirige al usuario a la página de inicio de sesión
    return redirect('/login')

def some_view_requiring_login(request):
    session_cookie = request.COOKIES.get('session')
    session = Session.objects.filter(Cookie=session_cookie).first()
    if session:
        user = session.User
        return user
    else:
        # El usuario no está autenticado, redirige a la página de inicio de sesión
        return redirect('/login')

def principal(request):
    user = some_view_requiring_login(request)
    if isinstance(user, HttpResponseRedirect):  # verifica si la variable user es una instancia de la clase HttpResponseRedirect
        return user  # Redirige directamente a la página de inicio de sesión
    info = barrainf()
    salas = Sala.objects.filter(sms__Author=user).distinct()  #Las salas en las que el usuario ha escrito.
    for sala in salas:
        sala.msg_count = SMS.objects.filter(Sala=sala).count()  # Cuenta el número total de mensajes en cada sala
        last_visit = UserSala.objects.filter(user=user, sala=sala).first()  # Obtiene la última visita del usuario a la sala
        if last_visit is not None:
            sala.new_msg_count = SMS.objects.filter(Sala=sala, Date__gt=last_visit.last_visited).count()  # GT=greater than Cuenta los mensajes nuevos desde la última visita
        else:
            sala.new_msg_count = sala.msg_count# Si el usuario nunca ha visitado la sala, todos los mensajes son nuevos
    if request.method == "GET":
        contex = {
            'salas': salas,
            **info
        }
        return render(request, 'principal.html', contex)
    elif request.method == 'POST':
        name_sala = request.POST.get('nombre-sala')
        if name_sala:
            existing_sala = Sala.objects.filter(Salas=name_sala).first()
            if existing_sala:
                contex = {
                    'salas': salas,
                    'error': 'Error: La sala ya existe',
                    **info
                }
                return render(request, 'principal.html', contex)
            else:
                if user is not None:
                    c = Sala(Salas=name_sala, Date=timezone.now(), Author=user)
                    c.save()
                    return redirect(f'/{name_sala}')
        else:
            contex = {
                'salas': salas,
                'error': 'Error: Debe proporcionar un nombre válido',
                **info
            }
            return render(request, 'principal.html', contex)
    else:
        return HttpResponseNotAllowed(['GET', 'POST'])

def arc_json(request, id_pagina):
    user = some_view_requiring_login(request)
    if isinstance(user,
                  HttpResponseRedirect):  # verifica si la variable user es una instancia de la clase HttpResponseRedirect
        return user
    try:
        sala = Sala.objects.get(Salas=id_pagina)
    except Sala.DoesNotExist:
        return HttpResponseNotFound('<h1>Página no encontrada</h1>')
    mensajes = SMS.objects.filter(Sala=sala).order_by('Date')
    mensajes_json = []
    for mensaje in mensajes:
        mensaje_data = {
            'author': mensaje.Author.User,
            'text': mensaje.Mensajes,
            'isimg': mensaje.Imagen != '',
            'date': mensaje.Date.strftime('%Y-%m-%d %H:%M:%S')
        }
        mensajes_json.append(mensaje_data)
    return JsonResponse(mensajes_json, safe=False)

def salas(request, id_pagina):
    info = barrainf()
    user = some_view_requiring_login(request)
    if isinstance(user, HttpResponseRedirect):
        return user
    if request.method == 'GET':
        try:
            sala = Sala.objects.get(Salas=id_pagina)
        except Sala.DoesNotExist:
            return HttpResponseNotFound('<h1>Página no encontrada</h1>')
        mensajes = SMS.objects.filter(Sala=sala)
        try:
            user_sala = UserSala.objects.get(user=user, sala=sala)
        except ObjectDoesNotExist:
            user_sala = UserSala(user=user, sala=sala)
        user_sala.last_visited = timezone.now()
        user_sala.save()
        context = {
            'sala': sala,
            'mensajes': mensajes,
            **info
        }
        return render(request, 'sala.html', context)
    elif request.method == 'POST':
        sala = Sala.objects.get(Salas=id_pagina)
        mensaje_texto = request.POST.get('mensaje')  # asumiendo que 'mensaje' es el nombre del campo en tu formulario
        imagen = request.FILES.get('imagen')  # Obtener la imagen enviada en el formulario

        nuevo_mensaje = SMS(Author=user, Mensajes=mensaje_texto, Sala=sala, Date=timezone.now())

        if imagen:
            file_path = default_storage.save('sms_images/' + imagen.name, imagen)
            nuevo_mensaje.Imagen = file_path

        nuevo_mensaje.save()

        mensajes = SMS.objects.filter(Sala=sala)
        context = {
            'sala': sala,
            'mensajes': mensajes,
            **info
        }
        return render(request, 'sala.html', context)

def help(request):
    user = some_view_requiring_login(request)
    if isinstance(user, HttpResponseRedirect):
        return user
    return render(request, 'help.html', barrainf())

def config(request):
    user = some_view_requiring_login(request)
    info = barrainf()
    if isinstance(user, HttpResponseRedirect):
        return user
    if request.method == 'POST':
        new_name = request.POST.get('nombre-charlador')
        word_size = request.POST.get('tamaño-letra')
        word_type = request.POST.get('tipo-letra')
        if new_name:
            if User.objects.filter(User=new_name).exists():
                contex = {
                    'error': 'Error: El usuario ya existe',
                    **info
                }
                return render(request, 'configuracion.html', contex)
            else:
                user.User = new_name  # Asigna el nuevo nombre al campo User
                user.save()
                contex = {
                    'msg': 'El nombre de usuario ha sido actualizado, recuerde que deberá iniciar sesión con el nuevo nombre.',
                    **info
                }
                return render(request, 'configuracion.html', contex)
        if word_size:
            request.session['font_size'] = word_size
            print(word_size)
        if word_type:
            request.session['font_family'] = word_type
            print(word_type)
            return render(request, 'configuracion.html', info)
        else:
            return render(request, 'configuracion.html', info)
    else:
        return render(request, 'configuracion.html', info)

